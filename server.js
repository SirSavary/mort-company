'use strict';

/**
 * Initialize environment variable
 */
var init = require('./config/init')();


/**
 * Module dependencies.
 */
var chalk = require('chalk');
var express = require('express');
var mongoose = require('mongoose');

/**
 * Main application entry file.
 * Please note that the order of loading is important.
 */

// Initializing system variables
var config = require('./config/config');

// Connect to the database
var db = mongoose.connect(config.db, function(err) {
  if (err) {
    console.error(chalk.red('Failed to connect to MongoDB. Please check db connection config.'));
    console.log(chalk.red(err));
  }
});

//express settings
var app = require('./config/express')(db);

// Bootstrap passport config
require('./config/passport')();

//Start the app by listening on <port>
var port = config.port;
app.listen(port);
console.log('Express app started on port ' + port);

//expose app
exports = module.exports = app;
