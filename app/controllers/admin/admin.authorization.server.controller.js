'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
  mongoose = require('mongoose'),
  Admin = mongoose.model('Admin');

/**
 * Admin middleware
 */
exports.adminByID = function(req, res, next, id) {
  Admin.findOne({
    _id: id
  })
    .exec(function(err, admin) {
      if (err) return next(err);
      if (!admin) return next(new Error('Failed to load Admin ' + id));
      req.profile = admin;
      next();
    });
};

/**
 * Require login routing middleware
 */
exports.requiresLogin = function(req, res, next) {
  if (!req.isAuthenticated()) {
    return res.status(401)
      .send({
        message: 'Admin is not logged in'
      });
  }

  next();
};
