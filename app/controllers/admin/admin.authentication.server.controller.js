'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
  errorHandler = require('../errors.server.controller'),
  mongoose = require('mongoose'),
  passport = require('passport'),
  Admin = mongoose.model('Admin');

/**
 * Signin after passport authentication
 */
exports.signin = function(req, res, next) {
  passport.authenticate('local', function(err, admin, info) {
    if (err || !admin) {
      res.status(400)
        .send(info);
    } else {
      // Remove sensitive data before login
      admin.password = undefined;
      admin.salt = undefined;

      req.login(admin, function(err) {
        if (err) {
          res.status(400)
            .send(err);
        } else {
          res.json(admin);
        }
      });
    }
  })(req, res, next);
};

/**
 * Signout
 */
exports.signout = function(req, res) {
  req.logout();
  res.redirect('/');
};
