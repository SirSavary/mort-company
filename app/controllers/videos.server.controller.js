'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var _ = require('lodash');
var errorHandler = require('./errors.server.controller');
var Video = mongoose.model('Video');

/**
 * Create a Video
 */
exports.create = function(req, res) {

};

/**
 * Show the current Video
 */
exports.read = function(req, res) {

};

/**
 * Update a Video
 */
exports.update = function(req, res) {

};

/**
 * Delete a Video
 */
exports.delete = function(req, res) {

};

/**
 * List of Videos
 */
exports.list = function(req, res) {
  Video
    .find()
    .sort('-created')
    .exec(function(err, videos) {
      if (err) {
        return res.status(400)
          .send({
            message: errorHandler.getErrorMessage(err)
          });
      } else {
        res.json(videos);
      }
    });
};
