'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  crypto = require('crypto');

/**
 * A Validation function for local strategy properties
 */
var validateLocalStrategyProperty = function(property) {
  return ((this.provider !== 'local' && !this.updated) || property.length);
};

/**
 * A Validation function for local strategy password
 */
var validateLocalStrategyPassword = function(password) {
  return (this.provider !== 'local' || (password && password.length > 6));
};

/**
 * Admin Schema
 */
var AdminSchema = new Schema({
  email: {
    type: String,
    trim: true,
    default: '',
    validate: [validateLocalStrategyProperty, 'Please fill in your email'],
    match: [/.+\@.+\..+/, 'Please fill a valid email address']
  },
  password: {
    type: String,
    default: '',
    validate: [validateLocalStrategyPassword, 'Password should be longer']
  },
  salt: {
    type: String
  },
  updated: {
    type: Date
  },
  created: {
    type: Date,
    default: Date.now
  }
});

/**
 * Hook a pre save method to hash the password
 *
 * FIXME, TODO: hash passwords
 */
AdminSchema.pre('save', function(next) {
  // if (this.password && this.password.length > 6) {
  //   this.salt = new Buffer(crypto.randomBytes(16)
  //     .toString('base64'), 'base64');
  //   this.password = this.hashPassword(this.password);
  // }

  next();
});

/**
 * Create instance method for hashing a password
 */
// AdminSchema.methods.hashPassword = function(password) {
//   if (this.salt && password) {
//     return crypto.pbkdf2Sync(password, this.salt, 10000, 64)
//       .toString('base64');
//   } else {
//     return password;
//   }
// };

/**
 * Create instance method for authenticating admin
 */
AdminSchema.methods.authenticate = function(password) {
  // return this.password === this.hashPassword(password);
  return this.password === password;
};

mongoose.model('Admin', AdminSchema);
