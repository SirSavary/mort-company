'use strict';

/**
 * Module dependencies
 */
var videos = require('../controllers/videos.server.controller');

module.exports = function(app) {
  // Routing logic   
  app.route('/videos')
    .get(videos.list);
};
