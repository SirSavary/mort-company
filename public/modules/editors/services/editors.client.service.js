'use strict';

//Editors service used to communicate Editors REST endpoints
angular.module('editors').factory('Editors', ['$resource',
	function($resource) {
		return $resource('editors/:editorId', { editorId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);