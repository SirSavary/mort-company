'use strict';

//Setting up route
angular.module('editors')
  .config(['$stateProvider',
    function($stateProvider) {
      // Editors state routing
      $stateProvider
        .state('listEditors', {
          url: '/editors',
          templateUrl: 'modules/editors/views/list-editors.client.view.html',
          admin: true
        })
        .state('createEditor', {
          url: '/editors/create',
          templateUrl: 'modules/editors/views/create-editor.client.view.html',
          admin: true
        })
        .state('viewEditor', {
          url: '/editors/:editorId',
          templateUrl: 'modules/editors/views/view-editor.client.view.html',
          admin: true
        })
        .state('editEditor', {
          url: '/editors/:editorId/edit',
          templateUrl: 'modules/editors/views/edit-editor.client.view.html',
          admin: true
        });
    }
  ])
  .run(['$rootScope', '$state', 'Authentication',
    function($rootScope, $state, Authentication) {
      $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {

        // if the state requires authentication and the admin is not logged in,
        // redirect to the login page
        if (toState.admin && !(Authentication.admin && Authentication.admin._id)) {
          $state.transitionTo('signin');
          event.preventDefault();
        }

      });
    }
  ]);
