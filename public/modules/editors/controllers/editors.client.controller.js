'use strict';

// Editors controller
angular.module('editors')
  .controller('EditorsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Editors',
    function($scope, $stateParams, $location, Authentication, Editors) {
      $scope.authentication = Authentication;

      // Create new Editor
      $scope.create = function() {
        // Create new Editor object
        var editor = new Editors({
          email: this.email,
          password: this.password
        });

        // Redirect after save
        editor.$save(function(response) {
          $location.path('editors');

          // Clear form fields
          $scope.email = '';
          $scope.password = '';
        }, function(errorResponse) {
          $scope.error = errorResponse.data.message;
        });
      };

      // Remove existing Editor
      $scope.remove = function(editor) {
        if (editor) {
          editor.$remove();

          for (var i in $scope.editors) {
            if ($scope.editors[i] === editor) {
              $scope.editors.splice(i, 1);
            }
          }
        } else {
          $scope.editor.$remove(function() {
            $location.path('editors');
          });
        }
      };

      // Update existing Editor
      $scope.update = function() {
        var editor = $scope.editor;

        editor.$update(function() {
          $location.path('editors');
        }, function(errorResponse) {
          $scope.error = errorResponse.data.message;
        });
      };

      // Find a list of Editors
      $scope.find = function() {
        $scope.editors = Editors.query();
      };

      // Find existing Editor
      $scope.findOne = function() {
        $scope.editor = Editors.get({
          editorId: $stateParams.editorId
        });
      };
    }
  ]);
