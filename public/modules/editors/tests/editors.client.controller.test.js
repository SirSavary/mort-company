'use strict';

(function() {
	// Editors Controller Spec
	describe('Editors Controller Tests', function() {
		// Initialize global variables
		var EditorsController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Editors controller.
			EditorsController = $controller('EditorsController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Editor object fetched from XHR', inject(function(Editors) {
			// Create sample Editor using the Editors service
			var sampleEditor = new Editors({
				name: 'New Editor'
			});

			// Create a sample Editors array that includes the new Editor
			var sampleEditors = [sampleEditor];

			// Set GET response
			$httpBackend.expectGET('editors').respond(sampleEditors);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.editors).toEqualData(sampleEditors);
		}));

		it('$scope.findOne() should create an array with one Editor object fetched from XHR using a editorId URL parameter', inject(function(Editors) {
			// Define a sample Editor object
			var sampleEditor = new Editors({
				name: 'New Editor'
			});

			// Set the URL parameter
			$stateParams.editorId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/editors\/([0-9a-fA-F]{24})$/).respond(sampleEditor);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.editor).toEqualData(sampleEditor);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Editors) {
			// Create a sample Editor object
			var sampleEditorPostData = new Editors({
				name: 'New Editor'
			});

			// Create a sample Editor response
			var sampleEditorResponse = new Editors({
				_id: '525cf20451979dea2c000001',
				name: 'New Editor'
			});

			// Fixture mock form input values
			scope.name = 'New Editor';

			// Set POST response
			$httpBackend.expectPOST('editors', sampleEditorPostData).respond(sampleEditorResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Editor was created
			expect($location.path()).toBe('/editors/' + sampleEditorResponse._id);
		}));

		it('$scope.update() should update a valid Editor', inject(function(Editors) {
			// Define a sample Editor put data
			var sampleEditorPutData = new Editors({
				_id: '525cf20451979dea2c000001',
				name: 'New Editor'
			});

			// Mock Editor in scope
			scope.editor = sampleEditorPutData;

			// Set PUT response
			$httpBackend.expectPUT(/editors\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/editors/' + sampleEditorPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid editorId and remove the Editor from the scope', inject(function(Editors) {
			// Create new Editor object
			var sampleEditor = new Editors({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Editors array and include the Editor
			scope.editors = [sampleEditor];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/editors\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleEditor);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.editors.length).toBe(0);
		}));
	});
}());