'use strict';

// Setting up route
angular.module('admin')
  .config(['$stateProvider',
    function($stateProvider) {
      // Admin state routing
      $stateProvider
        .state('signin', {
          url: '/admin/signin',
          templateUrl: 'modules/admin/views/authentication/signin.client.view.html'
        });
    }
  ]);
