'use strict';

// Authentication service for admin variables
angular.module('admin')
  .factory('Authentication', [

    function() {
      var _this = this;

      _this._data = {
        admin: window.admin
      };

      return _this._data;
    }
  ]);
