'use strict';

angular.module('admin')
  .controller('AuthenticationController', ['$scope', '$http', '$location', '$state', 'Authentication',
    function($scope, $http, $location, $state, Authentication) {
      $scope.authentication = Authentication;

      // If admin is signed in then redirect back home
      if ($scope.authentication.admin) $location.path('/');

      $scope.signin = function() {
        $http.post('/admin/signin', $scope.credentials)
          .success(function(response) {
            // If successful we assign the response to the global admin model
            $scope.authentication.admin = response;

            // And redirect to the editors list page
            $state.transitionTo('listEditors');
          })
          .error(function(response) {
            $scope.error = response.message;
          });
      };
    }
  ]);
