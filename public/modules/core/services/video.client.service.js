'use strict';

//Videos service used for fetching videos
angular.module('core')
  .service('VideoService', ["$q", "$http",
    function($q, $http) {

      // Vimeo settings
      this.VIMEO_VIDEO_URL = 'https://vimeo.com/';
      this.VIMEO_PICTURE_URL = 'https://i.vimeocdn.com/video/';
      this.VIMEO_PICTURE_URL_APPEND = '_960x540.jpg';


      var dummyFeaturedCategories = [

        {
          id: 1,
          description: "A brief description or tagline for this section will go here.",
          imgUrl: "/modules/core/img/placeholder.gif"
        },

        {
          id: 2,
          description: "A brief description or tagline for this section will go here.",
          imgUrl: "/modules/core/img/placeholder.gif"
        },

        {
          id: 3,
          description: "A brief description or tagline for this section will go here.",
          imgUrl: "/modules/core/img/placeholder.gif"
        }

      ];


      var dummyFeaturedVideo = {
        artist: "Loveless Movie",
        name: "Don't Call It A Comeback",
        vimeo_pic_id: 453779977,
        vimeo_id: 78374565
      };


      var dummyFilters = [

        {
          id: 1,
          name: "Stoner's Choice"
        },

        {
          id: 2,
          name: "Single Mom"
        },

        {
          id: 3,
          name: "Boring stuff for the rich"
        },

        {
          id: 4,
          name: "Take Revenge"
        },

        {
          id: 5,
          name: "Have Some Fun"
        },

        {
          id: 6,
          name: "Kill Your Boredom"
        }

      ];

      // var dummyVideos = [

      //   {
      //     id: 1,
      //     imgUrl: "https://i.vimeocdn.com/video/503052780_960x540.jpg",
      //     name: "Tall Tales",
      //     artist: "Kilogramme",
      //   },

      //   {
      //     id: 2,
      //     imgUrl: "https://i.vimeocdn.com/video/503052780_960x540.jpg",
      //     name: "Tall Tales 2",
      //     artist: "Kilogramme",
      //   },

      //   {
      //     id: 3,
      //     imgUrl: "https://i.vimeocdn.com/video/503052780_960x540.jpg",
      //     name: "Tall Tales 3",
      //     artist: "Kilogramme",
      //   },

      //   {
      //     id: 4,
      //     imgUrl: "https://i.vimeocdn.com/video/503052780_960x540.jpg",
      //     name: "Tall Tales 4",
      //     artist: "Kilogramme",
      //   },

      //   {
      //     id: 2,
      //     imgUrl: "https://i.vimeocdn.com/video/503052780_960x540.jpg",
      //     name: "Tall Tales 2",
      //     artist: "Kilogramme",
      //   },

      //   {
      //     id: 3,
      //     imgUrl: "https://i.vimeocdn.com/video/503052780_960x540.jpg",
      //     name: "Tall Tales 3",
      //     artist: "Kilogramme",
      //   },

      //   {
      //     id: 4,
      //     imgUrl: "https://i.vimeocdn.com/video/503052780_960x540.jpg",
      //     name: "Tall Tales 4",
      //     artist: "Kilogramme",
      //   },

      //   {
      //     id: 4,
      //     imgUrl: "https://i.vimeocdn.com/video/503052780_960x540.jpg",
      //     name: "Tall Tales 4",
      //     artist: "Kilogramme",
      //   },

      //   {
      //     id: 2,
      //     imgUrl: "https://i.vimeocdn.com/video/503052780_960x540.jpg",
      //     name: "Tall Tales 2",
      //     artist: "Kilogramme",
      //   },

      //   {
      //     id: 3,
      //     imgUrl: "https://i.vimeocdn.com/video/503052780_960x540.jpg",
      //     name: "Tall Tales 3",
      //     artist: "Kilogramme",
      //   },

      //   {
      //     id: 4,
      //     imgUrl: "https://i.vimeocdn.com/video/503052780_960x540.jpg",
      //     name: "Tall Tales 4",
      //     artist: "Kilogramme",
      //   },

      // ];



      this.getFeaturedCategories = function() {

        var deferred = $q.defer();

        // TODO: backend integration
        deferred.resolve(dummyFeaturedCategories);

        return deferred.promise;
      };

      this.getFeaturedVideo = function() {

        var deferred = $q.defer();
        // TODO: backend integration
        deferred.resolve(dummyFeaturedVideo);

        return deferred.promise;
      };


      this.getFilters = function() {

        var deferred = $q.defer();
        // TODO: backend integration
        deferred.resolve(dummyFilters);

        return deferred.promise;
      };

      this.getFilterVideos = function() {

        var deferred = $q.defer();

        $http.get('/videos')
          .success(function(videos) {
            deferred.resolve(videos);
          })
          .error(function(err) {
            deferred.reject(err);
          });



        return deferred.promise;
      };

    }
  ]);
