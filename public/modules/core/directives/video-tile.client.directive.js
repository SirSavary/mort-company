'use strict';

angular.module('core')
  .directive('videoTile', function() {
    return {
      scope: {
        video: '=videoTile'
      },
      restrict: 'EA',
      templateUrl: '/modules/core/views/video-partial.client.view.html'
    };
  });
