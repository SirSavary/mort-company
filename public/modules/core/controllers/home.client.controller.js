'use strict';


angular.module('core')
  .controller('HomeController', ['$scope', 'VideoService',

    function($scope, VideoService) {

      VideoService.getFeaturedCategories()
        .then(function(featuredCategories) {
          $scope.featuredCategories = featuredCategories;
        });

      VideoService.getFeaturedVideo()
        .then(function(featuredVideo) {
          $scope.featuredVideo = featuredVideo;
        });



      VideoService.getFilters()
        .then(function(filters) {
          $scope.filters = filters;
        });


      VideoService.getFilterVideos()
        .then(function(videos) {
          $scope.videos = videos;
        });


    }
  ]);

