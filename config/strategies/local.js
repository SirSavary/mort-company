'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport'),
  LocalStrategy = require('passport-local')
    .Strategy,
  Admin = require('mongoose')
    .model('Admin');

module.exports = function() {
  // Use local strategy
  passport.use(new LocalStrategy({
      usernameField: 'email',
      passwordField: 'password'
    },
    function(username, password, done) {
      Admin.findOne({
        email: username
      }, function(err, admin) {
        if (err) {
          return done(err);
        }
        if (!admin) {
          return done(null, false, {
            message: 'Unknown admin or invalid password'
          });
        }
        if (!admin.authenticate(password)) {
          return done(null, false, {
            message: 'Unknown admin or invalid password'
          });
        }

        return done(null, admin);
      });
    }
  ));
};
